<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;

use App\Models\ContributionBatch;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\Contribution;
use App\Models\Fund;
use App\Models\Member;
use App\Models\PaymentMethod;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class ContributionController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Sentinel::hasAccess('contributions.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Contribution::all();

        return view('contribution.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Sentinel::hasAccess('contributions.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $batches = array();
        // $batches["0"] = trans_choice('new', 1) . ' ' . trans_choice('batch', 1);
        foreach (ContributionBatch::where('status', 0)->get() as $key) {
            $batches[$key->id] = $key->id . ' -' . $key->name;
        }
        $funds = array();
        foreach (Fund::all() as $key) {
            $funds[$key->id] = $key->name;
        }
        $payment_methods = array();
        foreach (PaymentMethod::all() as $key) {
            $payment_methods[$key->id] = $key->name;
        }
        $members = array();
        foreach (Member::all() as $key) {
            $members[$key->id] = $key->first_name . ' ' . $key->middle_name . ' ' . $key->last_name . '(' . $key->id . ')';
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'contributions')->get();
        return view('contribution.create', compact('batches', 'custom_fields', 'funds', 'payment_methods', 'members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Sentinel::hasAccess('contributions.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }

        $contribution = new Contribution();
        if (!empty($request->member_type)) {
            $contribution->member_type = 0;
        } else {
            $contribution->member_type = 1;
            $contribution->member_id = $request->member_id;
        }

        $contribution->fund_id = $request->fund_id;
        $contribution->family_id = $request->family_id;
        $contribution->contribution_batch_id = $request->contribution_batch_id;
        $contribution->payment_method_id = $request->payment_method_id;
        $contribution->amount = $request->amount;
        $contribution->notes = $request->notes;
        $contribution->date = $request->date;
        $date = explode('-', $request->date);
        $contribution->year = $date[0];
        $contribution->month = $date[1];
        $files = array();
        if (!empty(array_filter($request->file('files')))) {
            $count = 0;
            foreach ($request->file('files') as $key) {
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }
                $count++;
            }
        }
        $contribution->files = serialize($files);
        $contribution->save();
        $custom_fields = CustomField::where('category', 'contribution')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $contribution->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "contribution";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Added contribution with id:" . $contribution->id);
        Flash::success(trans('general.successfully_saved'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        if (!empty($request->save_return)) {
            return redirect()->back();
        }
        if (!empty($request->save_exit)) {
            return redirect('contribution/data');
        }
        return redirect('contribution/data');
    }


    public function show($contribution)
    {
        if (!Sentinel::hasAccess('contributions.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'contributions')->get();
        return view('contribution.show', compact('contribution', 'user', 'custom_fields'));
    }


    public function edit($contribution)
    {
        if (!Sentinel::hasAccess('contributions.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $batches = array();
        // $batches["0"] = trans_choice('new', 1) . ' ' . trans_choice('batch', 1);
        foreach (ContributionBatch::where('status', 0)->get() as $key) {
            $batches[$key->id] = $key->id . ' -' . $key->name;
        }
        $funds = array();
        foreach (Fund::all() as $key) {
            $funds[$key->id] = $key->name;
        }
        $payment_methods = array();
        foreach (PaymentMethod::all() as $key) {
            $payment_methods[$key->id] = $key->name;
        }
        $members = array();
        foreach (Member::all() as $key) {
            $members[$key->id] = $key->first_name . ' ' . $key->middle_name . ' ' . $key->last_name . '(' . $key->id . ')';
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'contributions')->get();
        return view('contribution.edit',
            compact('contribution', 'batches', 'custom_fields', 'funds', 'payment_methods', 'members'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('contributions.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $contribution = Contribution::find($id);
        if (!empty($request->member_type)) {
            $contribution->member_type = 0;
        } else {
            $contribution->member_type = 1;
            $contribution->member_id = $request->member_id;
        }
        $contribution->fund_id = $request->fund_id;
        $contribution->family_id = $request->family_id;
        $contribution->contribution_batch_id = $request->contribution_batch_id;
        $contribution->payment_method_id = $request->payment_method_id;
        $contribution->amount = $request->amount;
        $contribution->notes = $request->notes;
        $contribution->date = $request->date;
        $date = explode('-', $request->date);
        $contribution->year = $date[0];
        $contribution->month = $date[1];
        $files = unserialize($contribution->files);
        $count = count($files);
        if (!empty(array_filter($request->file('files')))) {
            foreach ($request->file('files') as $key) {
                $count++;
                $file = array('files' => $key);
                $rules = array('files' => 'required|mimes:jpeg,jpg,bmp,png,pdf,docx,xlsx');
                $validator = Validator::make($file, $rules);
                if ($validator->fails()) {
                    Flash::warning(trans('general.validation_error'));
                    return redirect()->back()->withInput()->withErrors($validator);
                } else {
                    $files[$count] = $key->getClientOriginalName();
                    $key->move(public_path() . '/uploads',
                        $key->getClientOriginalName());
                }

            }
        }
        $contribution->files = serialize($files);
        $contribution->save();
        $custom_fields = CustomField::where('category', 'contributions')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'contributions')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'contributions')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "contributions";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Updated contribution with id:" . $contribution->id);
        Flash::success(trans('general.successfully_saved'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        return redirect('contribution/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        if (!Sentinel::hasAccess('contributions.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        Contribution::destroy($id);
        GeneralHelper::audit_trail("Deleted contribution with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        return redirect('contribution/data');
    }

    public function deleteFile(Request $request, $id)
    {
        if (!Sentinel::hasAccess('contributions.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $contribution = Contribution::find($id);
        $files = unserialize($contribution->files);
        @unlink(public_path() . '/uploads/' . $files[$request->id]);
        $files = array_except($files, [$request->id]);
        $contribution->files = serialize($files);
        $contribution->save();


    }

}
