<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;

use App\Models\Campaign;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Member;
use App\Models\Pledge;
use App\Models\PledgePayment;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class PledgeController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Sentinel::hasAccess('pledges')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = Pledge::all();

        return view('pledge.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Sentinel::hasAccess('pledges.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $campaigns = array();
        foreach (Campaign::where('status', 0)->get() as $key) {
            $campaigns[$key->id] = $key->name;
        }
        $members = array();
        foreach (Member::all() as $key) {
            $members[$key->id] = $key->first_name . ' ' . $key->middle_name . ' ' . $key->last_name . '(' . $key->id . ')';
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'pledges')->get();
        return view('pledge.create', compact('campaigns', 'custom_fields', 'members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Sentinel::hasAccess('pledges.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $pledge = new Pledge();
        $pledge->user_id = Sentinel::getUser()->id;
        $pledge->member_id = $request->member_id;
        $pledge->campaign_id = $request->campaign_id;
        $pledge->amount = $request->amount;
        $pledge->notes = $request->notes;
        $pledge->date = $request->date;
        $date = explode('-', $request->date);
        $pledge->recurring = $request->recurring;
        if ($request->recurring == 1) {
            $pledge->recur_frequency = $request->recur_frequency;
            $pledge->recur_start_date = $request->recur_start_date;
            if (!empty($request->recur_end_date)) {
                $pledge->recur_end_date = $request->recur_end_date;
            }

            $pledge->recur_next_date = date_format(date_add(date_create($request->recur_start_date),
                date_interval_create_from_date_string($request->recur_frequency . ' ' . $request->recur_type . 's')),
                'Y-m-d');

            $pledge->recur_type = $request->recur_type;
        }
        $pledge->year = $date[0];
        $pledge->month = $date[1];
        $pledge->save();
        $custom_fields = CustomField::where('category', 'pledges')->get();
        foreach ($custom_fields as $key) {
            $custom_field = new CustomFieldMeta();
            $id = $key->id;
            $custom_field->name = $request->$id;
            $custom_field->parent_id = $pledge->id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "pledges";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Added pledge with id:" . $pledge->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('pledge/data');
    }


    public function show($pledge)
    {
        if (!Sentinel::hasAccess('pledges.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'pledges')->get();
        return view('pledge.show', compact('pledge', 'custom_fields'));
    }


    public function edit($pledge)
    {
        if (!Sentinel::hasAccess('pledges.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $campaigns = array();
        foreach (Campaign::where('status', 0)->get() as $key) {
            $campaigns[$key->id] = $key->name;
        }
        $members = array();
        foreach (Member::all() as $key) {
            $members[$key->id] = $key->first_name . ' ' . $key->middle_name . ' ' . $key->last_name . '(' . $key->id . ')';
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'pledges')->get();
        return view('pledge.edit', compact('pledge', 'campaigns', 'custom_fields', 'members'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('pledges.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $pledge = Pledge::find($id);
        $pledge->member_id = $request->member_id;
        $pledge->campaign_id = $request->campaign_id;
        $pledge->amount = $request->amount;
        $pledge->notes = $request->notes;
        $pledge->date = $request->date;
        $date = explode('-', $request->date);
        $pledge->recurring = $request->recurring;
        if ($request->recurring == 1) {
            $pledge->recur_frequency = $request->recur_frequency;
            $pledge->recur_start_date = $request->recur_start_date;
            if (!empty($request->recur_end_date)) {
                $pledge->recur_end_date = $request->recur_end_date;
            }
            if (empty($pledge->recur_next_date)) {
                $pledge->recur_next_date = date_format(date_add(date_create($request->recur_start_date),
                    date_interval_create_from_date_string($request->recur_frequency . ' ' . $request->recur_type . 's')),
                    'Y-m-d');
            }
            $pledge->recur_type = $request->recur_type;
        }
        $pledge->year = $date[0];
        $pledge->month = $date[1];
        $pledge->save();
        $custom_fields = CustomField::where('category', 'pledges')->get();
        foreach ($custom_fields as $key) {
            if (!empty(CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id', $id)->where('category',
                'pledges')->first())
            ) {
                $custom_field = CustomFieldMeta::where('custom_field_id', $key->id)->where('parent_id',
                    $id)->where('category', 'pledges')->first();
            } else {
                $custom_field = new CustomFieldMeta();
            }
            $kid = $key->id;
            $custom_field->name = $request->$kid;
            $custom_field->parent_id = $id;
            $custom_field->custom_field_id = $key->id;
            $custom_field->category = "pledges";
            $custom_field->save();
        }
        GeneralHelper::audit_trail("Updated pledge with id:" . $pledge->id);
        Flash::success(trans('general.successfully_saved'));
        return redirect('pledge/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!Sentinel::hasAccess('pledges.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        Pledge::destroy($id);
        PledgePayment::where('pledge_id', $id)->delete();
        GeneralHelper::audit_trail("Deleted pledge with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        return redirect('pledge/data');
    }


}
