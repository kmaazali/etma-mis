<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\GeneralHelper;

use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\Pledge;
use App\Models\PledgePayment;
use App\Models\Fund;
use App\Models\Member;
use App\Models\PaymentMethod;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class PledgePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Sentinel::hasAccess('pledges.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $data = PledgePayment::all();

        return view('pledge_payment.data', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (!Sentinel::hasAccess('pledges.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $payment_methods = array();
        foreach (PaymentMethod::all() as $key) {
            $payment_methods[$key->id] = $key->name;
        }
        $members = array();
        foreach (Member::all() as $key) {
            $members[$key->id] = $key->first_name . ' ' . $key->middle_name . ' ' . $key->last_name . '(' . $key->id . ')';
        }
        //get custom fields
        return view('pledge_payment.create', compact('id', 'payment_methods', 'members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (!Sentinel::hasAccess('pledges.create')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $pledge = Pledge::find($id);
        $payment = new PledgePayment();
        $payment->member_id = $pledge->member_id;
        $payment->pledge_id = $id;
        $payment->payment_method_id = $request->payment_method_id;
        $payment->amount = $request->amount;
        $payment->notes = $request->notes;
        $payment->date = $request->date;
        $date = explode('-', $request->date);
        $payment->year = $date[0];
        $payment->month = $date[1];
        $payment->save();
        GeneralHelper::audit_trail("Added pledge payment with id:" . $payment->id);
        Flash::success(trans('general.successfully_saved'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        return redirect('pledge/data');
    }


    public function show($pledge_payment)
    {
        if (!Sentinel::hasAccess('pledges.view')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $users = User::all();
        $user = array();
        foreach ($users as $key) {
            $user[$key->id] = $key->first_name . ' ' . $key->last_name;
        }
        //get custom fields
        $custom_fields = CustomField::where('category', 'contributions')->get();
        return view('pledge_payment.show', compact('pledge_payment', 'user', 'custom_fields'));
    }


    public function edit($pledge_payment)
    {
        if (!Sentinel::hasAccess('pledges.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $payment_methods = array();
        foreach (PaymentMethod::all() as $key) {
            $payment_methods[$key->id] = $key->name;
        }
        //get custom fields
        return view('pledge_payment.edit',
            compact('pledge_payment', 'payment_methods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Sentinel::hasAccess('pledges.update')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $payment = PledgePayment::find($id);
        $payment->payment_method_id = $request->payment_method_id;
        $payment->amount = $request->amount;
        $payment->notes = $request->notes;
        $payment->date = $request->date;
        $date = explode('-', $request->date);
        $payment->year = $date[0];
        $payment->month = $date[1];
        $payment->save();
        GeneralHelper::audit_trail("Updated pledge payment with id:" . $payment->id);
        Flash::success(trans('general.successfully_saved'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        return redirect('pledge/data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if (!Sentinel::hasAccess('pledges.delete')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        PledgePayment::destroy($id);
        GeneralHelper::audit_trail("Deleted pledge payment with id:" . $id);
        Flash::success(trans('general.successfully_deleted'));
        if (isset($request->return_url)) {
            return redirect($request->return_url);
        }
        return redirect()->back();
    }


}
