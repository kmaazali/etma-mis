<?php

namespace App\Http\Controllers;

use Aloha\Twilio\Twilio;
use App\Helpers\BulkSms;
use App\Helpers\GeneralHelper;
use App\Models\Borrower;

use App\Models\Collateral;
use App\Models\CollateralType;
use App\Models\Contribution;
use App\Models\CustomField;
use App\Models\CustomFieldMeta;
use App\Models\Expense;
use App\Models\ExpenseType;
use App\Models\Loan;
use App\Models\LoanRepayment;
use App\Models\LoanSchedule;
use App\Models\OtherIncome;
use App\Models\Payroll;
use App\Models\PledgePayment;
use App\Models\SavingTransaction;
use App\Models\Setting;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Clickatell\Api\ClickatellHttp;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cash_flow(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenses = GeneralHelper::total_expenses($request->start_date, $request->end_date);
        $payroll = GeneralHelper::total_payroll($request->start_date, $request->end_date);
        $contributions = GeneralHelper::total_contributions($request->start_date, $request->end_date);
        $other_income = GeneralHelper::total_other_income($request->start_date, $request->end_date);
        $pledges = GeneralHelper::total_pledges_payments($request->start_date, $request->end_date);

        $total_payments = $expenses + $payroll;
        $total_receipts = $pledges + $contributions + $other_income;
        $cash_balance = $total_receipts - $total_payments;
        return view('report.cash_flow',
            compact('expenses', 'payroll', 'contributions', 'total_payments', 'other_income', 'pledges',
                'interest_paid', 'fees_paid', 'penalty_paid', 'total_receipts', 'cash_balance', 'start_date',
                'end_date'));
    }

    public function profit_loss(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $expenses = GeneralHelper::total_expenses($request->start_date, $request->end_date);
        $payroll = GeneralHelper::total_payroll($request->start_date, $request->end_date);
        $contributions = GeneralHelper::total_contributions($request->start_date, $request->end_date);
        $other_income = GeneralHelper::total_other_income($request->start_date, $request->end_date);
        $pledges = GeneralHelper::total_pledges_payments($request->start_date, $request->end_date);
        $operating_expenses = $expenses + $payroll;
        $operating_profit = $contributions + $pledges + $other_income;
        $gross_profit = $operating_profit - $operating_expenses;
        $net_profit = $gross_profit;
        //build graphs here
        $monthly_net_income_data = array();
        $monthly_operating_profit_expenses_data = array();
        $monthly_overview_data = array();
        if (isset($request->end_date)) {
            $date = $request->end_date;
        } else {
            $date = date("Y-m-d");
        }
        $start_date1 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        $start_date2 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        $start_date3 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date1);
            $o_profit = Contribution::where('year', $d[0])->where('month',
                    $d[1])->sum('amount') + OtherIncome::where('year', $d[0])->where('month',
                    $d[1])->sum('amount') + PledgePayment::where('year', $d[0])->where('month', $d[1])->sum('amount');
            $o_expense = Expense::where('year', $d[0])->where('month',
                $d[1])->sum('amount');
            foreach (Payroll::where('year', $d[0])->where('month',
                $d[1])->get() as $key) {
                $o_expense = $o_expense + GeneralHelper::single_payroll_total_pay($key->id);
            }

            $ext = ' ' . $d[0];
            $n_income = $o_profit - $o_expense;
            array_push($monthly_net_income_data, array(
                'month' => date_format(date_create($start_date1),
                    'M' . $ext),
                'amount' => $n_income

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date2);
            //get loans in that period
            $o_profit = Contribution::where('year', $d[0])->where('month',
                    $d[1])->sum('amount') + OtherIncome::where('year', $d[0])->where('month',
                    $d[1])->sum('amount') + PledgePayment::where('year', $d[0])->where('month', $d[1])->sum('amount');
            $o_expense = Expense::where('year', $d[0])->where('month',
                $d[1])->sum('amount');
            foreach (Payroll::where('year', $d[0])->where('month',
                $d[1])->get() as $key) {
                $o_expense = $o_expense + GeneralHelper::single_payroll_total_pay($key->id);
            }

            $ext = ' ' . $d[0];
            array_push($monthly_operating_profit_expenses_data, array(
                'month' => date_format(date_create($start_date2),
                    'M' . $ext),
                'profit' => $o_profit,
                'expenses' => $o_expense

            ));
            //add 1 month to start date
            $start_date2 = date_format(date_add(date_create($start_date2),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date3);
            //get loans in that period
            $contributions = Contribution::where('year', $d[0])->where('month',
                    $d[1])->sum('amount') ;
            $pledges=PledgePayment::where('year', $d[0])->where('month', $d[1])->sum('amount');
            $other_income=OtherIncome::where('year', $d[0])->where('month',
                $d[1])->sum('amount');

            $ext = ' ' . $d[0];
            array_push($monthly_overview_data, array(
                'month' => date_format(date_create($start_date3),
                    'M' . $ext),
                'contributions' => $contributions,
                'pledges' => $pledges,
                'other_income' => $other_income,
            ));
            //add 1 month to start date
            $start_date3 = date_format(date_add(date_create($start_date3),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }
        $monthly_net_income_data = json_encode($monthly_net_income_data);
        $monthly_operating_profit_expenses_data = json_encode($monthly_operating_profit_expenses_data);
        $monthly_overview_data = json_encode($monthly_overview_data);
        return view('report.profit_loss',
            compact('expenses', 'payroll', 'operating_expenses', 'other_income',
                'contributions', 'pledges', 'operating_profit', 'gross_profit', 'start_date',
                'end_date', 'net_profit', 'monthly_net_income_data',
                'monthly_operating_profit_expenses_data', 'monthly_overview_data', 'other_expenses'));
    }

    public function collection_report(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        if (isset($request->end_date)) {
            $date = $request->end_date;
        } else {
            $date = date("Y-m-d");
        }
        $monthly_collections = array();
        $start_date1 = date_format(date_sub(date_create($date),
            date_interval_create_from_date_string('1 years')),
            'Y-m-d');
        for ($i = 1; $i < 14; $i++) {
            $d = explode('-', $start_date1);
            //get loans in that period
            $payments = 0;
            $payments_due = 0;
            foreach (Loan::where('year', $d[0])->where('month', $d[1])->where('loan_status', '!=',
                'defaulted')->get() as $key) {
                $payments = $payments + GeneralHelper::loan_paid_item($key->id, 'interest',
                        $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'fees',
                        $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'penalty',
                        $key->due_date) + GeneralHelper::loan_paid_item($key->id, 'principal', $key->due_date);
                $payments_due = $payments_due + GeneralHelper::loan_total_principal($key->id) + GeneralHelper::loan_total_fees($key->id) + GeneralHelper::loan_total_penalty($key->id) + GeneralHelper::loan_total_interest($key->id);
            }
            $payments = round($payments, 2);
            $payments_due = round($payments_due, 2);

            if ($i == 1 or $i == 13) {
                $ext = ' ' . $d[0];
            } else {
                $ext = '';
            }
            array_push($monthly_collections, array(
                'month' => date_format(date_create($start_date1),
                    'M' . $ext),
                'payments' => $payments,
                'due' => $payments_due

            ));
            //add 1 month to start date
            $start_date1 = date_format(date_add(date_create($start_date1),
                date_interval_create_from_date_string('1 months')),
                'Y-m-d');
        }
        $monthly_collections = json_encode($monthly_collections);
        return view('report.collection',
            compact('start_date', 'end_date', 'monthly_collections'));
    }

    public function balance_sheet(Request $request)
    {
        if (!Sentinel::hasAccess('reports')) {
            Flash::warning("Permission Denied");
            return redirect('/');
        }
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        //$capital = GeneralHelper::total_capital($request->start_date, $request->end_date);
        /* $expenses = GeneralHelper::total_expenses($request->start_date, $request->end_date);
         $payroll = GeneralHelper::total_payroll($request->start_date, $request->end_date);
         $principal = GeneralHelper::loans_total_principal($request->start_date, $request->end_date);
         $other_income = GeneralHelper::total_other_income($request->start_date, $request->end_date);*/
        // $deposits = GeneralHelper::total_savings_deposits($request->start_date, $request->end_date);
        //$withdrawals = GeneralHelper::total_savings_withdrawals($request->start_date, $request->end_date);
        /*$principal_paid = GeneralHelper::loans_total_paid_item('principal', $request->start_date, $request->end_date);
        $interest_paid = GeneralHelper::loans_total_paid_item('interest', $request->start_date, $request->end_date);
        $fees_paid = GeneralHelper::loans_total_paid_item('fees', $request->start_date, $request->end_date);
        $penalty_paid = GeneralHelper::loans_total_paid_item('penalty', $request->start_date, $request->end_date);*/
        //$total_payments = $expenses + $payroll + $principal + $withdrawals;
        //$total_receipts = $principal_paid + $fees_paid + $interest_paid + $penalty_paid + $other_income + $deposits+$capital;
        //$cash_balance = $total_receipts - $total_payments;
        return view('report.balance_sheet',
            compact('expenses', 'payroll', 'principal', 'total_payments', 'other_income', 'principal_paid',
                'interest_paid', 'fees_paid', 'penalty_paid', 'total_receipts', 'cash_balance', 'start_date',
                'end_date', 'withdrawals', 'deposits', 'capital'));
    }

}
