@extends('layouts.master')
@section('title')
    {{ trans_choice('general.add',1) }} {{ trans_choice('general.contribution',1) }}
@endsection

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans_choice('general.add',1) }} {{ trans_choice('general.contribution',1) }}</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        {!! Form::open(array('url' => url('contribution/store'), 'method' => 'post','class'=>'', 'name' => 'form',"enctype"=>"multipart/form-data")) !!}
        <div class="box-body">
            <p class="bg-navy disabled color-palette">{{ trans_choice('general.required',1) }} {{ trans_choice('general.field',2) }}</p>
            <div class="form-group">
                {!! Form::label('member_id',trans_choice('general.member',1),array('class'=>' control-label')) !!}
                <div id="memberDetails">
                    {!! Form::select('member_id',$members,null, array('class' => 'form-control select2','placeholder'=>'','required'=>'required','id'=>'member_id')) !!}
                </div>
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="member_type" value="1"
                               id="member_type"> {{ trans('general.anonymous') }}
                    </label>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('contribution_batch_id',trans_choice('general.batch',1),array('class'=>' control-label')) !!}

                {!! Form::select('contribution_batch_id',$batches,null, array('class' => 'form-control select2','placeholder'=>'','required'=>'required','id'=>'contribution_batch_id')) !!}
            </div>

            <div class="form-group">
                {!! Form::label('amount',trans_choice('general.amount',1),array('class'=>'')) !!}
                {!! Form::text('amount',null, array('class' => 'form-control touchspin', 'placeholder'=>"",'required'=>'required')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('date',trans_choice('general.date',1),array('class'=>'')) !!}
                {!! Form::text('date',null, array('class' => 'form-control date-picker', 'placeholder'=>"",'required'=>'required')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('fund_id',trans_choice('general.fund',1),array('class'=>' control-label')) !!}

                {!! Form::select('fund_id',$funds,null, array('class' => 'form-control select2','placeholder'=>'','required'=>'required','id'=>'fund_id')) !!}

            </div>
            <div class="form-group">
                {!! Form::label('payment_method_id',trans_choice('general.payment',1).' '.trans_choice('general.method',1),array('class'=>' control-label')) !!}

                {!! Form::select('payment_method_id',$payment_methods,null, array('class' => 'form-control select2','placeholder'=>'','required'=>'required','id'=>'payment_method_id')) !!}

            </div>
            <p class="bg-navy disabled color-palette">{{ trans_choice('general.optional',1) }} {{ trans_choice('general.field',2) }}</p>
            <div class="form-group">
                {!! Form::label('notes',trans_choice('general.note',2),array('class'=>'')) !!}
                {!! Form::textarea('notes',null, array('class' => 'form-control')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('files',trans_choice('general.file',2).'('.trans_choice('general.borrower_file_types',1).')',array('class'=>'')) !!}
                {!! Form::file('files[]', array('class' => 'form-control', 'multiple'=>"",'rows'=>'3')) !!}
                <div class="col-sm-12">{{trans_choice('general.select_thirty_files',1)}}
                </div>
            </div>
            <div class="form-group">
                <hr>
            </div>
            <p class="bg-navy disabled color-palette">{{trans_choice('general.custom_field',2)}}</p>
            @foreach($custom_fields as $key)

                <div class="form-group">
                    {!! Form::label($key->id,$key->name,array('class'=>'')) !!}
                    @if($key->field_type=="number")
                        <input type="number" class="form-control" name="{{$key->id}}"
                               @if($key->required==1) required @endif>
                    @endif
                    @if($key->field_type=="textfield")
                        <input type="text" class="form-control" name="{{$key->id}}"
                               @if($key->required==1) required @endif>
                    @endif
                    @if($key->field_type=="date")
                        <input type="text" class="form-control date-picker" name="{{$key->id}}"
                               @if($key->required==1) required @endif>
                    @endif
                    @if($key->field_type=="textarea")
                        <textarea class="form-control" name="{{$key->id}}"
                                  @if($key->required==1) required @endif></textarea>
                    @endif
                    @if($key->field_type=="decimal")
                        <input type="text" class="form-control touchspin" name="{{$key->id}}"
                               @if($key->required==1) required @endif>
                    @endif
                </div>
            @endforeach
            <p style="text-align:center; font-weight:bold;">
                <small><a href="{{url('custom_field/create')}}" target="_blank">Click here to add custom fields on
                        this page</a></small>
            </p>

        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary margin pull-right" name="save_return" value="save_return">{{ trans_choice('general.save',1) }} & {{ trans_choice('general.return',1) }} </button>
            <button type="submit" class="btn btn-primary margin pull-right" name="save_exit" value="save_exit">{{ trans_choice('general.save',1) }} & {{ trans_choice('general.exit',1) }}</button>
        </div>
    {!! Form::close() !!}
    <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('footer-scripts')
    <script>
        $("#member_type").on('ifChecked', function (e) {
            $("#member_id").removeAttr('required');
            $("#memberDetails").hide();

        });
        $("#member_type").on('ifUnchecked', function (e) {
            $("#member_id").attr('required', 'required');
            $("#memberDetails").show();
        });
        $(document).ready(function (e) {

        })

    </script>
@endsection

