@extends('layouts.master')
@section('title')
    {{trans_choice('general.member',2)}}
@endsection
@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans_choice('general.member',2)}}</h3>

            <div class="box-tools pull-right">
                @if(Sentinel::hasAccess('members.create'))
                    <a href="{{ url('member/create') }}"
                       class="btn btn-info btn-sm">{{trans_choice('general.add',1)}} {{trans_choice('general.member',1)}}</a>
                @endif
            </div>
        </div>
        <div class="box-body ">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered table-condensed table-hover">
                    <thead>
                    <tr style="background-color: #D1F9FF">
                        <th>{{trans_choice('general.id',1)}}</th>
                        <th>{{trans_choice('general.name',1)}}</th>
                        <th>{{trans_choice('general.photo',1)}}</th>
                        <th>{{trans_choice('general.phone',1)}}</th>
                        <th>{{trans_choice('general.gender',1)}}</th>
                        <th>{{trans_choice('general.age',1)}}</th>
                        <th>{{trans_choice('general.address',1)}}</th>
                        <th>{{ trans_choice('general.action',1) }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key)
                        <tr>
                            <td>#{{$key->id}}</td>
                            <td>
                                <a href="{{url('member/'.$key->id.'/show')}}">{{$key->first_name}} {{$key->middle_name}} {{$key->last_name}}</a>
                            </td>
                            <td>
                                @if(!empty($key->photo))
                                    <a class="fancybox" rel="group"
                                       href="{{ url(asset('uploads/'.$key->photo)) }}"> <img
                                                src="{{ url(asset('uploads/'.$key->photo)) }}" width="120"/></a>
                                @else
                                    <img class="img-thumbnail"
                                         src="{{asset('assets/dist/img/user.png')}}"
                                         alt="user image" style="max-height: 100px!important;"/>
                                @endif
                            </td>
                            <td>{{$key->mobile_phone}}</td>
                            <td>
                                @if($key->gender=="male")
                                    {{trans_choice('general.male',1)}}
                                @endif
                                @if($key->gender=="female")
                                    {{trans_choice('general.female',1)}}
                                @endif
                                @if($key->gender=="unknown")
                                    {{trans_choice('general.unknown',1)}}
                                @endif
                            </td>
                            <td>
                                @if(!empty($key->dob))
                                    {{date("Y-m-d")-$key->dob}}
                                @endif
                            </td>
                            <td>{!! $key->address !!}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-xs dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        {{ trans('general.choose') }} <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                        @if(Sentinel::hasAccess('members.view'))
                                            <li><a href="{{ url('member/'.$key->id.'/show') }}"><i
                                                            class="fa fa-search"></i> {{trans_choice('general.detail',2)}}
                                                </a></li>
                                        @endif
                                        @if(Sentinel::hasAccess('members.update'))
                                            <li><a href="{{ url('member/'.$key->id.'/edit') }}"><i
                                                            class="fa fa-edit"></i> {{ trans('general.edit') }} </a>
                                            </li>
                                        @endif
                                        @if(Sentinel::hasAccess('members.delete'))
                                            <li><a href="{{ url('member/'.$key->id.'/delete') }}" class="delete"><i
                                                            class="fa fa-trash"></i> {{ trans('general.delete') }} </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection
@section('footer-scripts')
    <script src="{{ asset('assets/plugins/datatable/media/js/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/media/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/datatable/extensions/Buttons/js/buttons.colVis.min.js')}}"></script>
    <script>
        $('#data-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {extend: 'copy', 'text': '{{ trans('general.copy') }}'},
                {extend: 'excel', 'text': '{{ trans('general.excel') }}'},
                {extend: 'pdf', 'text': '{{ trans('general.pdf') }}'},
                {extend: 'print', 'text': '{{ trans('general.print') }}'},
                {extend: 'csv', 'text': '{{ trans('general.csv') }}'},
                {extend: 'colvis', 'text': '{{ trans('general.colvis') }}'}
            ],
            "paging": true,
            "lengthChange": true,
            "displayLength": 15,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "order": [[0, "asc"]],
            "columnDefs": [
                {"orderable": false, "targets": [5]}
            ],
            "language": {
                "lengthMenu": "{{ trans('general.lengthMenu') }}",
                "zeroRecords": "{{ trans('general.zeroRecords') }}",
                "info": "{{ trans('general.info') }}",
                "infoEmpty": "{{ trans('general.infoEmpty') }}",
                "search": "{{ trans('general.search') }}",
                "infoFiltered": "{{ trans('general.infoFiltered') }}",
                "paginate": {
                    "first": "{{ trans('general.first') }}",
                    "last": "{{ trans('general.last') }}",
                    "next": "{{ trans('general.next') }}",
                    "previous": "{{ trans('general.previous') }}"
                }
            },
            responsive: false
        });
    </script>
@endsection
